<ul class="nav flex-column">
  <li class="nav-item">
   <a class="nav-link @if(request()->is('*review*')) active @endif" href="{{url('review')}}">クチコミ管理</a>
  </li>
  <li class="nav-item">
   <a class="nav-link @if(request()->is('*template*')) active @endif" href="{{url('template')}}">テンプレート管理</a>
  </li>
</ul>
