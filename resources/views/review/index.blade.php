@extends('layouts.app')
@section('title', 'クチコミ管理')
@section('content')
<h3>クチコミ管理</h3>
<form action="{{url('/review')}}" method="get" name="review">
 <div class="row m-3">
  <div class="col-5 m-3">
   <label for="review_st_date">クチコミ登録日(始)</label>
   <input id="reviewStDate" type="text" placeholder="" class="mb-3 form-control flatpickr{{ $errors->has('stDate') ? ' is-invalid' : ''}}" name="stDate" value="{{ request()->input('stDate') }}">
   @if ($errors->has('stDate'))
     <span class="invalid-feedback" role="alert">
         <strong>{{ $errors->first('stDate') }}</strong>
     </span>
   @endif
   <label for="review_end_date">クチコミ登録日(終)</label>
   <input id="reviewEndDate" type="text" placeholder="" class="form-control flatpickr{{ $errors->has('endDate') ? ' is-invalid' : ''}}" name="endDate" value="{{ request()->input('endDate') }}">
   @if ($errors->has('endDate'))
     <span class="invalid-feedback" role="alert">
         <strong>{{ $errors->first('endDate') }}</strong>
     </span>
   @endif
  </div>
  <div class="col-5 m-3">
   <label for="rate">クチコミ評点</label>
   {{Form::select('rate', config('formConst.rate'), request()->input('rate'), ['class' => 'mb-3 form-control','placeholder' => '選択してください'])}}
   <label for="replyStatus">返信ステータス</label>
   {{Form::select('replyStatus', config('formConst.replyStatus'), request()->input('replyStatus'), ['class' => 'form-control','placeholder' => '選択してください'])}}
  </div>
 </div>
</form>
 <div class="row m-3">
  <div class="col-3 m-3">
   <button class="btn btn-success btn-block" type="button" onclick="document.review.submit();">検索</button>
  </div>
  <div class="col-3 m-3">
   <form action="{{url('/review/export')}}" method="post" onSubmit="exportPrepare()">
    @csrf
    <input type="hidden" id="exportStDate" name="stDate"  value="">
    <input type="hidden" id="exportEndDate" name="endDate" value="">
    <button class="btn btn-primary btn-block" type="submit">エクスポート</button>
   </form>
  </div>
 </div>
<div class="table-responsive">
<table class="table table-striped table-sm">
  <thead>
   <tr>
    <th>ID</th>
    <th>日付</th>
    <th>クチコミ評点</th>
    <th>ユーザ名</th>
    <th>タイトル</th>
    <th>返信ステータス</th>
   </tr>
  </thead>
  <tbody>
  @forelse($reviews as $review)
  <tr>
   <td><a href="{{url('review/reply/'.$review->review_id)}}">{{$review->review_id}}</a></td>
   <td>{{$review->gmb_create_date->format('Y/m/d')}}</td>
   <td>
    {{mb_str_pad($review->gmb_star_rating,config('const.ACTIVE_RATE_STRING'),config('const.RATE_LIMIT'), config('const.INACTIVE_RATE_STRING'))}}</td>
   <td>{{$review->gmb_name}}</td>
   <td>{{Str::limit($review->gmb_comment,config('const.REVIEW_COMMENT_LIMIT'),"...")}}</td>
   <td>@if(is_null($review->review_reply_id)) 未 @else 済 @endif</td>
  </tr>
  @empty
  @endforelse
  </tbody>
 </table>
 {{$reviews->appends(request()->input())->links()}}
</div>
@endsection
<script type="text/javascript" src="/js/review.js"></script>