@extends('layouts.app')
@section('title', 'クチコミ対応 返信')
@section('content')
<div class="col-md-8 order-md-1">
 <h4 class="mb-4">クチコミ対応 返信</h4>
 <form action="{{url('/review/reply/post')}}" method="post">
    @csrf
  <input type="hidden" name="review_id" value="{{$review->review_id}}">
  <input type="hidden" name="review_reply_id" value="{{$review->review_reply_id}}">
  <div class="mb-4">
     <label for="" class="d-block font-weight-bold">クチコミ日時</label>
     {{$review->gmb_create_date->format('Y/m/d')}}
  </div>

  <div class="mb-4">
     <label for="" class="d-block font-weight-bold">クチコミユーザ名</label>
     {{$review->gmb_name}} <img src="{{$review->gmb_profile_photo_url}}" class="ml-3">
   </div>

  <div class="mb-4">
     <label for="" class="d-block font-weight-bold">クチコミ評点</label>
     {{mb_str_pad($review->gmb_star_rating,config('const.ACTIVE_RATE_STRING'),config('const.RATE_LIMIT'), config('const.INACTIVE_RATE_STRING'))}}
   </div>

  <div class="mb-4">
     <label for="" class="d-block font-weight-bold">クチコミ</label>
     {{$review->gmb_comment}}
  </div>

  <div class="mb-4">
     <label for="" class="font-weight-bold">テンプレート選択</label>
     {{Form::select('template', $templates,old('template'),['id'=>'template','onChange'=>'getTemplate()','class'=>'form-control','placeholder' => '選択してください'])}}
  </div>

  <div class="mb-4">
     <label for="gmb_comment" class="font-weight-bold">返信内容</label>
     {{Form::textarea('gmb_comment', old('gmb_comment',(!empty($reply->gmb_comment)) ? $reply->gmb_comment:''),
     ['rows' => 8,'id'=>'gmb_comment','class'=>['form-control',($errors->has('gmb_comment')) ? 'is-invalid':'']])}}
     @if ($errors->has('gmb_comment'))
         <span class="invalid-feedback" role="alert">
             <strong>{{ $errors->first('gmb_comment') }}</strong>
         </span>
     @endif
  </div>

   <div class="row p-3">
    <div class="mx-auto w-25">
        <button class="btn btn-primary btn-block" type="submit">返信する</button>
    </div>
   </div>

 </form>
</div>
@endsection
<script type="text/javascript" src="/js/review.js"></script>