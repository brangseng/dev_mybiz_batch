@extends('layouts.app')
@section('title', '返信テンプレート一覧')
@section('content')
<h3>返信テンプレート一覧</h3>
<form action="{{url('/template')}}" method="get" name="review">
 <div class="row mr-3 ml-3 pt-3">
  <div class="col-3 mr-5">
   <label for="" class="d-block">返信タイプ</label>
   <label class="form-check-label radio-inline mr-4" for="is_autoreply_template1">
     {{Form::radio('is_autoreply_template', config('const.FLG_ON'), (request()->input('is_autoreply_template') == config('const.FLG_ON')), ['class'=>''])}}自動返信
   </label>
   <label class="form-check-label radio-inline" for="is_autoreply_template2">
    {{Form::radio('is_autoreply_template', config('const.FLG_OFF'), (request()->input('is_autoreply_template') == config('const.FLG_OFF')), ['class'=>''])}}手動返信
   </label>
  </div>
  <div class="col-5">
   <label for="rate">自動返信クチコミ評点</label>
   {{Form::select('target_star_rating', config('formConst.rate'), request()->input('target_star_rating'), ['class' => 'mb-3 form-control','placeholder' => '選択してください'])}}
  </div>
 </div>
 <div class="row mr-3 ml-3 col-3 mb-5 mt-2">
   <button class="btn btn-success btn-block" type="submit">検索</button>
 </div>
</form>
<div class="row col-3 mb-3">
 <button class="btn btn-primary btn-block" onclick="location.href='/template/create';">新規追加</button>
</div>
 <div class="table-responsive">
  <table class="table table-striped table-sm">
   <thead>
    <tr>
     <th>ID</th>
     <th>テンプレート名</th>
     <th>タイプ</th>
     <th>クチコミ評点</th>
     <th></th>
    </tr>
   </thead>
   <tbody>
  @forelse($templates as $template)
  <tr>
   <td><a href="{{url('template/'.$template->review_reply_template_id)}}">{{$template->review_reply_template_id}}</a></td>
   <td>{{$template->template_name}}</td>
   <td>
    @if(isset(config('formConst.IS_AUTO_REPLY')[$template->is_autoreply_template]))
     {{config('formConst.IS_AUTO_REPLY')[$template->is_autoreply_template]}}
    @endif
   </td>
   <td>
   {{mb_str_pad($template->target_star_rating,config('const.ACTIVE_RATE_STRING'),config('const.RATE_LIMIT'), config('const.INACTIVE_RATE_STRING'))}}
   </td>
   <td>
    <button class="btn btn-secondary btn-block" onclick="deleteTemplate({{$template->review_reply_template_id}})">削除</button>
   </td>
  </tr>
  @empty
  @endforelse
  </tbody>
 </table>
 </div>
@endsection
<script type="text/javascript" src="/js/review_template.js"></script>