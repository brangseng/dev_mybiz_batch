<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_replies', function (Blueprint $table) {
            $table->unsignedInteger('review_reply_id')->autoIncrement();
            $table->string('gmb_comment',1000)->nullable();
            $table->dateTime('gmb_update_time')->nullable();
            $table->tinyInteger('is_deleted')->nullable();
            $table->enum('sync_status',
                ['DRAFT', 'QUEUED', 'SYNCED'])->nullable();
            $table->dateTime('sync_time')->nullable();
            $table->char('create_user_id', 10)->nullable();
            $table->dateTime('create_time')->nullable();
            $table->char('update_user_id', 10)->nullable();
            $table->dateTime('update_time')->nullable();
            //$table->primary('review_reply_id');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE review_replies MODIFY review_reply_id int(11) unsigned NOT NULL AUTO_INCREMENT');
        DB::connection()->getPdo()->exec('ALTER TABLE review_replies MODIFY is_deleted tinyint(1) unsigned NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_replies');
    }
}
