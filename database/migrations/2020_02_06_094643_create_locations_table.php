<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->unsignedInteger('location_id');
            $table->string('gmb_name',100)->nullable();
            $table->string('gmb_language_code',100)->nullable();
            $table->string('gmb_store_code',100)->nullable();
            $table->string('gmb_location_name' ,100)->nullable();
            $table->string('gmb_primary_phone',16)->nullable();
            $table->string('gmb_additional_phones_1',16)->nullable();
            $table->string('gmb_additional_phones_2' ,16)->nullable();
            $table->unsignedInteger('primary_category_id');
            $table->string('gmb_website_url' ,100)->nullable();
            $table->primary(['location_id', 'primary_category_id']);
            $table->index('primary_category_id','fk_locations_categories1_idx');
            $table->foreign('primary_category_id','fk_locations_categories1')
                ->references('category_id')
                ->on('categories');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE locations MODIFY location_id int(11) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE locations MODIFY primary_category_id int(11) unsigned NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
