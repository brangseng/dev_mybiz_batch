<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->unsignedInteger('review_id');
            $table->string('gmb_name',100);
            $table->string('gmb_reviewid',100);
            $table->string('gmb_profile_photo_url',100);
            $table->string('gmb_display_name',100);
            $table->unsignedTinyInteger('gmb_is_anonymous');
            $table->unsignedTinyInteger('gmb_star_rating');
            $table->string('gmb_comment',1000);
            $table->dateTime('gmb_create_date');
            $table->dateTime('gmb_update_date');
            $table->unsignedTinyInteger('is_deleted')->nullable();
            $table->dateTime('sync_time')->nullable();
            $table->unsignedInteger('review_reply_id')->nullable();
            $table->primary('review_id');
            $table->index('review_reply_id','fk_reviews_review_reply1_idx');
            $table->foreign('review_reply_id','fk_reviews_review_reply1')
                ->references('review_reply_id')
                ->on('review_replies');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE reviews MODIFY review_id int(11) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE reviews MODIFY gmb_is_anonymous tinyint(1) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE reviews MODIFY gmb_star_rating tinyint(1) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE reviews MODIFY is_deleted tinyint(1) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE reviews MODIFY review_reply_id int(11) unsigned NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
