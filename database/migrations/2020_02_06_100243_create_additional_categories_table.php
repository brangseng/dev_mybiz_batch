<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionalCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_categories', function (Blueprint $table) {
            $table->unsignedInteger('location_id');
            $table->unsignedInteger('category_id');
            $table->primary('location_id');
            $table->index('category_id','fk_additionalcategories_categories1_idx');
            $table->foreign('location_id','fk_additionalcategories_locations1')
                ->references('location_id')
                ->on('locations');
            $table->foreign('category_id','fk_additionalcategories_categories1')
                ->references('category_id')
                ->on('categories');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE additional_categories MODIFY location_id int(11) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE additional_categories MODIFY category_id int(11) unsigned NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_categories');
    }
}
