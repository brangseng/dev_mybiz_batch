<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_hours', function (Blueprint $table) {
            $table->unsignedInteger('locations_location_id');
            $table->primary('locations_location_id');
            $table->foreign('locations_location_id','fk_special_hours_locations1')
                ->references('location_id')
                ->on('locations');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE special_hours MODIFY locations_location_id int(11) unsigned NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_hours');
    }
}
