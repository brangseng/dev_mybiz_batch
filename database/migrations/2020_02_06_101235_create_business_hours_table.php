<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_hours', function (Blueprint $table) {
            $table->unsignedInteger('location_id');
            $table->enum('gmb_open_day',
                ['DAY_OF_WEEK_UNSPECIFIED', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'])
            ->nullable();
            $table->time('gmb_open_time')->nullable();
            $table->enum('gmb_close_day',
                ['DAY_OF_WEEK_UNSPECIFIED', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'])
                ->nullable();
            $table->time('gmb_close_time')->nullable();
            $table->primary('location_id');
            $table->foreign('location_id','fk_business_hours_locations1')
                ->references('location_id')
                ->on('locations');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE business_hours MODIFY location_id int(11) unsigned NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_hours');
    }
}
