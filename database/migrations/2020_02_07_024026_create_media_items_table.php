<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_items', function (Blueprint $table) {
            $table->unsignedInteger('mediaitem_id');
            $table->string('gmb_name',100)->nullable();
            $table->enum('gmb_media_format',
                ['MEDIA_FORMAT_UNSPECIFIED', 'PHOTO', 'VIDEO'])->nullable();
            $table->enum('gmb_location_association_category',
                ['CATEGORY_UNSPECIFIED', 'COVER', 'PROFILE', 'LOGO', 'EXTERIOR', 'INTERIOR',
                 'PRODUCT', 'AT_WORK', 'FOOD_AND_DRINK', 'MENU', 'COMMON_AREA', 'ROOMS',
                 'TEAMS', 'ADDITIONAL'])->nullable();
            $table->string('gmb_location_association_price_list_item_id' ,100)
                ->nullable();
            $table->string('gmb_google_url' ,100)->nullable();
            $table->string('gmb_thumbnail_url' ,100)->nullable();
            $table->dateTime('gmb_create_time')->nullable();
            $table->unsignedInteger('gmb_dimentions_width_pixels');
            $table->unsignedInteger('gmb_dimentions_height_pixels');
            $table->unsignedInteger('gmb_insights_view_count');
            $table->string('gmb_attribution_profile_name' ,100)->nullable();
            $table->string('gmb_attribution_profile_photo_url' ,100)->nullable();
            $table->string('gmb_attribution_takedown_url' ,100)->nullable();
            $table->string('gmb_atttribution_profile_url' ,100)->nullable();
            $table->string('gmb_description' ,1000)->nullable();
            $table->string('gmb_source_url' ,100)->nullable();
            $table->string('gmb_data_ref_media_item_data_ref_resource_name' ,100)
                ->nullable();
            $table->string('s3_object_url' ,100)->nullable();
            $table->tinyInteger('is_deleted')->nullable();
            $table->enum('sync_status',
                ['DRAFT', 'READY', 'QUEUED', 'SYNCED'])->nullable();
            $table->dateTime('sync_time')->nullable();
            $table->char('create_user_id', 10)->nullable();
            $table->dateTime('create_time')->nullable();
            $table->char('update_user_id', 10)->nullable();
            $table->dateTime('update_time')->nullable();
            $table->primary('mediaitem_id');
        });
        DB::connection()->getPdo()->exec('ALTER TABLE media_items MODIFY mediaitem_id int(11) unsigned NOT NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE media_items MODIFY gmb_dimentions_width_pixels int(11) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE media_items MODIFY gmb_dimentions_height_pixels int(11) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE media_items MODIFY gmb_insights_view_count int(11) unsigned NULL');
        DB::connection()->getPdo()->exec('ALTER TABLE media_items MODIFY is_deleted tinyint(1) unsigned NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_items');
    }
}
