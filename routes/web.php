<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth:web'], function () {
    Route::get('/', 'IndexController@index');
    Route::get('/review', 'ReviewController@index');
    Route::post('/review/export', 'ReviewController@export');
    Route::get('/review/reply/{reviewId}', 'ReviewController@reply')->where('reviewId', '[0-9]+');
    Route::post('/review/reply/post', 'ReviewController@post');

    Route::get('/template', 'ReviewReplyTemplateController@index');
    Route::get('/template/create', 'ReviewReplyTemplateController@create');
    Route::post('/template/store', 'ReviewReplyTemplateController@store');
    Route::get('/template/{reviewReplyTemplateId}', 'ReviewReplyTemplateController@edit')
        ->where('reviewReplyTemplateId', '[0-9]+');
    Route::post('/template/update', 'ReviewReplyTemplateController@update');
    Route::post('/template/delete', 'ReviewReplyTemplateController@delete');
    Route::post('/template/async/find', 'ReviewReplyTemplateController@asyncFind');
});