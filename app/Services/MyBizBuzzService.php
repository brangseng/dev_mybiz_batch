<?php
// 修正
namespace App\Services;

use DB;
use App\Account;
use App\Location;
use App\Review;
use App\LogApi;
use App\Services\MyBizService;
use \Illuminate\Database\QueryException;

use Carbon\Carbon;

class MyBizBuzzService
{
    private $_proc_exit;
    private $_kubun;
    private $_class_function;
    private $_detail;
    private $_exception;
    private $_review_count;
    private $_review_new_count;
    private $_review_reply_count;
    private $_started_at;
    private $_ended_at;

    public function __construct()
    {
        // for debug
        DB::enableQueryLog();
    }

    // クチコミを抽出してS3にCSVデータとして格納
    public function analyseReviews($myBizService, $from, $to, $gmbAccountId, $gmbLocationId)
    { 

        $this->_kubun = 1;
        $this->_class_function = "MyBizBuzzService.analyseReviews";
        $this->_detail = "";
        $this->_exception = "";
        $this->_started_at = Carbon::now();
        $this->_review_count = 0;
        $this->_review_new_count = 0;
        $this->_review_reply_count = 0;
        $this->_review_auto_reply_count = 0;

        if ($gmbAccountId == NULL && $gmbLocationId == NULL) {
            // 契約企業全てのブランド・店舗のクチコミを取得
            $accounts = Account::select(['gmb_account_id'])
                                ->active()
                                ->where('gmb_account_id', '<>', '102356320813998189642')
                                ->get();

            foreach ($accounts as $account) {
                $locations = Location::select(['gmb_location_id'])
                                    ->active()
                                    ->where('gmb_account_id', '=', $account['gmb_account_id'])
                                    ->get();

                foreach ($locations as $location) {
                    $this->_getLocationReviews($myBizService, $from, $to, $account['gmb_account_id'], $location['gmb_location_id']);
                    unset($location);
                }
                unset($locations);
            }
            unset($accounts);

        } else if ($gmbLocationId == NULL) {
            // 指定されたブランド配下の全店舗のクチコミを取得
            $locations = Location::select(['gmb_location_id'])
                                ->active()
                                ->where('gmb_account_id', '=', $gmbAccountId)
                                ->get();

            foreach ($locations as $location) {
                $this->_getLocationReviews($myBizService, $from, $to, $gmbAccountId, $location['gmb_location_id']);
                unset($location);
            }
            unset($locations);

        } else {
            // 指定された店舗のクチコミを取得
            $this->_getLocationReviews($myBizService, $from, $to, $gmbAccountId, $gmbLocationId);
        }

        /*
        // ログ出力
        $this->_proc_exit = 0;
        $this->_exception = "";
        $this->_detail = sprintf("review_count=%d, review_new_count=%d, review_reply_count=%d, review_auto_reply_count=%d", 
                                $this->_review_count, $this->_review_new_count, $this->_review_reply_count, $this->_review_auto_reply_count);
        $this->_logging($gmbApiService);
        */
     //   logger()->error(DB::getQueryLog());
    }

    // 店舗のクチコミを取得
    private function _getLocationReviews($myBizService, $from, $to, $gmbAccountId, $gmbLocationId)
    { 

        try {

            $location = Location::select(['account_id', 'location_id'])
                                ->active()
                                ->where('gmb_account_id', '=', $gmbAccountId)
                                ->where('gmb_location_id', '=', $gmbLocationId)
                                ->first();

            if ($location != null) {
                $reviews = Review::active()
                                    ->where('gmb_account_id', '=', $gmbAccountId)
                                    ->where('gmb_location_id', '=', $gmbLocationId)
                                    ->where('gmb_comment', '<>', '')
                                    ->where('gmb_create_time', '>=', $from.' 00:00:00')
                                    ->where('gmb_create_time', '<=', $from.' 23:59:59')
                                    ->orderBy('gmb_location_id', 'asc')
                                    ->orderBy('gmb_create_time', 'asc')
                                    ->get();

                if ($reviews) {
                    foreach($reviews as $review){
                        
                        // 句読点で分割
                        $comment = str_replace(",", "、", $review['gmb_comment']);
                        $comment = str_replace("！", "。", $review['gmb_comment']);
                        $comment = str_replace("!", "。", $review['gmb_comment']);
                        $sentenceAry = explode("。",$comment);

                        $count = 1;
                        foreach ($sentenceAry as $s) {
                            if (strlen($s) < 2) continue;

                            $line = '';
                            $line .= $review['review_id'] .',';
                            $line .= $review['gmb_account_id'] .',';
                            $line .= $review['gmb_location_id'] .',';
                            $line .= $review['gmb_reviewer_display_name'] .',';
                            $line .= $review['gmb_star_rating'] .',';
                            $line .= $s .',';
                            $line .= $review['gmb_create_time'] .',';
                            $count++;

                            print($line);
                        }
                    }
                }
                unset($reviews);
            }

        }catch(Exception $e){
            print($e->getMessage());

            /*
            $this->_proc_exit = -1;
            $this->_class_function = "GmbApiReviewQueryService._getLocationReviews";
            $this->_detail = sprintf("name=%s", 'accounts/'.$gmbAccountId .'/locations/' .$gmbLocationId);
            $this->_exception = $e->getMessage();
            $this->_logging($gmbApiService);
            */

        } finally {
            // 開放
            $location = null;
            $reviews = null;

            $this->_debug(DB::getQueryLog());
        }

    }

    // ログ出力
    private function _logging($gmbApiService) {
        $this->_ended_at = Carbon::now();
        $gmbApiService->logApiBatch($this->_kubun, 
                                    $this->_proc_exit,
                                    $this->_class_function,
                                    $this->_detail,
                                    $this->_exception,
                                    $this->_started_at,
                                    $this->_ended_at);
    }
    
    private function _debug($msg) {
        var_dump($msg);
    }

  }