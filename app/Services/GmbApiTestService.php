<?php
// 修正
namespace App\Services;

use DB;
use App\Account;
use App\Location;
use App\Review;
use App\ReviewReply;
use App\LogApi;
use App\Services\GmbApiService;
use \Illuminate\Database\QueryException;

use Google_Client;
use Google_Service_MyBusiness_Location;
use Google_Service_MyBusiness_Profile;
use Google_Service_MyBusiness_LocalPost;
use Google_Service_MyBusiness_CallToAction;
use Google_Service_MyBusiness_LocalPostEvent;
use Google_Service_MyBusiness_Date;
use Google_Service_MyBusiness_TimeOfDay;
use Google_Service_MyBusiness_TimeInterval;
use Google_Service_MyBusiness_LocalPostOffer;
use Google_Service_MyBusiness_LocalPostProduct;
use Google_Service_MyBusiness_MediaItem;
use Google_Service_MyBusiness_LocationAssociation;
use Google_Service_MyBusiness_Review;
use Google_Service_MyBusiness_ReviewReply;
use Google_Service_Exception;
use Carbon\Carbon;

class GmbApiTestService
{
    private $_proc_exit;
    private $_kubun;
    private $_class_function;
    private $_detail;
    private $_exception;
    private $_account_count;
    private $_started_at;
    private $_ended_at;
    
    public function __construct()
    {
        // for debug
     //   DB::enableQueryLog();
    }


    // 過去の特定のレビューに対して返信する
    public function mytestReviewReply($gmbService, $gmbApiService)
    { 
        $this->_debug('mytestReviewReply');
    }


    public function test1($gmbService, $gmbApiService)
    { 
        print('test1');
        /*
        try {

            $reviewReplies = ReviewReply::select(['reviews.review_id'
                                                , 'reviews.gmb_account_id'
                                                , 'reviews.gmb_location_id'
                                                , 'reviews.gmb_review_id'
                                                , 'review_replies.review_reply_id'
                                                , 'review_replies.sync_type'
                                                , 'review_replies.sync_status'
                                                , 'review_replies.gmb_comment'])
                                    ->join('reviews', function ($join) {
                                        $join->on('reviews.review_id', '=', 'review_replies.review_id')
                                            ->where('reviews.is_deleted', '=', config('const.IS_DELETED.OFF'));
                                    })
                                    ->queued()
                                    ->where('review_replies.scheduled_sync_time', '<=', Carbon::now())
                                    ->orderBy('reviews.gmb_account_id', 'asc')
                                    ->orderBy('reviews.gmb_location_id', 'asc')
                                    ->get();

            foreach($reviewReplies as $rs){
                print($rs);

                // 同期成功
                $this->_saveReviewReply($gmbService, $gmbApiService, config('const.SYNC_STATUS.SYNCED'), $rs);

                break;
            }

        }catch(Exception $e){
    
        } finally {
        }
        */
    }

    private function _saveReviewReply($gmbService, $gmbApiService, $sync_status, $rs)
    { 
        /*
        try {
            
            $reviewReply = ReviewReply::where('review_reply_id', '=', $rs["review_reply_id"])
                                        ->first();

            if ($reviewReply != null) {

                DB::beginTransaction();

                $review_id = $reviewReply->review_id;
            
                $reviewReply->sync_status  = $sync_status;
                if ($sync_status != config('const.SYNC_STATUS.FAILED')) {
                    $reviewReply->scheduled_sync_time = null;
                }
                $reviewReply->sync_time  = Carbon::now();
                $reviewReply->update();


                $review = Review::where('review_id', '=', $review_id)
                                ->first();

                if ($review != null) {

                    $review->sync_status  = $sync_status;
                    if ($sync_status != config('const.SYNC_STATUS.FAILED')) {
                        $review->scheduled_sync_time = null;
                        // 過去のクチコミに返信した場合も、返信ステータス＝済にする 2021.06.18
                        $review->gmb_review_reply_comment = $rs["gmb_comment"];
                        $review->gmb_review_reply_update_time = Carbon::now();
                    }
                    $review->sync_time  = Carbon::now();
                    $review->update();
                }

                DB::commit();
            }

        }catch(Exception $e){
            DB::rollBack();

            $this->_proc_exit = -1;
            $this->_class_function = "GmbApiReviewRegistService._saveReviewReply";
            $this->_detail = sprintf("review_reply_id=%d", $rs["review_reply_id"]);
            $this->_exception = $e->getMessage();
            $this->_logging($gmbApiService);

        } finally {
            // 開放
            $reviewReply = null;
        }
        */
    }


    // ログ出力
    private function _logging($gmbApiService) {
        $this->_ended_at = Carbon::now();
        $gmbApiService->logApiBatch($this->_kubun, 
                                    $this->_proc_exit,
                                    $this->_class_function,
                                    $this->_detail,
                                    $this->_exception,
                                    $this->_started_at,
                                    $this->_ended_at);
    }
    
    private function _debug($msg) {
        var_dump($msg);
    }
  }