<?php
// 修正
namespace App\Services;

use DB;
use App\LogApi;

use App\Account;
use App\Location;
use App\Review;
use \Illuminate\Database\QueryException;

use Carbon\Carbon;

class MyBizService
{
    public function __construct()
    {
    }

    // ログ出力
    public function logBatch($kubun, 
                                $proc_exit,
                                $class_function,
                                $detail,
                                $exception,
                                $started_at,
                                $ended_at) {

        try {

            $logApi = new LogApi;
            $logApi->kubun          = $kubun;
            $logApi->proc_exit      = $proc_exit;
            $logApi->class_function = $class_function;
            $logApi->detail         = $detail;
            $logApi->exception      = $exception;
            $logApi->started_at     = $started_at;
            $logApi->ended_at       = $ended_at;

            $logApi->save();

        } catch ( Exception $e ) {
            logger()->error(sprintf("logApiBatch Exception: %s", $e->getMessage()));
        } finally {
            $logApi = null;
        }
    }

    public function covertTimezone2Jst($value) {
        $datetime = new Carbon($value, 'UTC');
        $datetime->setTimezone('JST');
        return $datetime;
    }

    public function checkGmbJson($value, $default='') {

        try {
            if ($value == NULL || $value == 'NULL') $value = $default;
            else {
                if (is_array($value)) {
                    $value = implode(",", str_replace(',', '、', $value));
                }
            }

        } catch ( Exception $e ) {   
            $value = $default;
        }

        return $value;
    }

    public function checkBooleanGmbJson($value) {

        try {

            if (isset($value)) {
                $value = (int)$value;
            } else {
                $value = null;
            }

        } catch ( Exception $e ) {   
            $value = null;
        }

        return $value;
    }

    public function removeTranslatedGoogleString($src) {
        $comment = trim($this->checkGmbJson($src));
        if(strpos($comment,'(Translated by Google)') !== false){
            $comment = trim(stristr($comment, "(Translated by Google)", true));
        }
        return $comment;
    }

    public function checkGmbJsonScheduleTime($date, $time) {

        $datetime = NULL;
        $fmtDate = '';
        $fmtTime = '00:00:00';
        if ($date['year'] != NULL && $date['month'] != NULL && $date['day'] != NULL) {
            if (is_numeric($date['year']) && is_numeric($date['month']) && is_numeric($date['day'])) {
                $format = '%04d-%02d-%02d';
                $fmtDate = sprintf($format, $date['year'], $date['month'], $date['day']);
            }
        }

        if ($time['hours'] != NULL && $time['minutes'] != NULL && $time['seconds'] != NULL) {
            if (is_numeric($time['hours']) && is_numeric($time['minutes']) && is_numeric($time['seconds'])) {
                $format = '%02d:%02d:%02d';
                $fmtTime = sprintf($format, $time['hours'], $time['minutes'], $time['seconds']);
            }
        }

        if ($fmtDate != '') {
            $datetime = DB::raw("STR_TO_DATE('" .$fmtDate  ." " .$fmtTime ."','%Y-%m-%d %H:%i:%s')");
        }

        return $datetime;
    }

    // 日付型データを文字列に変換する
    public function convDatetime2String($datetime) {

        $ary = [];
        if($datetime != NULL && strlen($datetime) == 19) {
            if (substr($datetime, 0, 4) != "0000") {
                $ary['year'] = substr($datetime, 0, 4);
                $ary['month'] = substr($datetime, 5, 2);
                $ary['day'] = substr($datetime, 8, 2);

                $ary['hour'] = substr($datetime, 11, 2);
                $ary['minute'] = substr($datetime, 14, 2);
                $ary['second'] = substr($datetime, 17, 2);
            }
        }

        return $ary;
    }

    public function debug($msg) {
        var_dump($msg);
    }

  }