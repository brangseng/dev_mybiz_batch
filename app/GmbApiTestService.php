<?php
// 修正
namespace App\Services;

use DB;
use App\Account;
use App\Location;
use App\Review;
use App\ReviewReply;
use App\LogApi;
use App\Services\GmbApiService;
use \Illuminate\Database\QueryException;

use Google_Client;
use Google_Service_MyBusiness_Location;
use Google_Service_MyBusiness_Profile;
use Google_Service_MyBusiness_LocalPost;
use Google_Service_MyBusiness_CallToAction;
use Google_Service_MyBusiness_LocalPostEvent;
use Google_Service_MyBusiness_Date;
use Google_Service_MyBusiness_TimeOfDay;
use Google_Service_MyBusiness_TimeInterval;
use Google_Service_MyBusiness_LocalPostOffer;
use Google_Service_MyBusiness_LocalPostProduct;
use Google_Service_MyBusiness_MediaItem;
use Google_Service_MyBusiness_LocationAssociation;
use Google_Service_MyBusiness_Review;
use Google_Service_MyBusiness_ReviewReply;
use Google_Service_Exception;
use Carbon\Carbon;

class GmbApiTestService
{
    private $_proc_exit;
    private $_kubun;
    private $_class_function;
    private $_detail;
    private $_exception;
    private $_account_count;
    private $_started_at;
    private $_ended_at;
    
    public function __construct()
    {
        // for debug
     //   DB::enableQueryLog();
    }

    public function test1($gmbService, $gmbApiService)
    { 
        print('test1');

        try {

            $reviewReplies = ReviewReply::select(['reviews.review_id'
                                                , 'reviews.gmb_account_id'
                                                , 'reviews.gmb_location_id'
                                                , 'reviews.gmb_review_id'
                                                , 'review_replies.review_reply_id'
                                                , 'review_replies.sync_type'
                                                , 'review_replies.sync_status'
                                                , 'review_replies.gmb_comment'])
                                    ->join('reviews', function ($join) {
                                        $join->on('reviews.review_id', '=', 'review_replies.review_id')
                                            ->where('reviews.is_deleted', '=', config('const.IS_DELETED.OFF'));
                                    })
                                    ->queued()
                                    ->where('review_replies.scheduled_sync_time', '<=', Carbon::now())
                                    ->orderBy('reviews.gmb_account_id', 'asc')
                                    ->orderBy('reviews.gmb_location_id', 'asc')
                                    ->get();


            foreach($reviewReplies as $rs){
                print($rs);


                break;
            }

        }catch(Exception $e){
    
        } finally {
        }

    }

    // ログ出力
    private function _logging($gmbApiService) {
        $this->_ended_at = Carbon::now();
        $gmbApiService->logApiBatch($this->_kubun, 
                                    $this->_proc_exit,
                                    $this->_class_function,
                                    $this->_detail,
                                    $this->_exception,
                                    $this->_started_at,
                                    $this->_ended_at);
    }
    
    private function _debug($msg) {
        var_dump($msg);
    }
  }