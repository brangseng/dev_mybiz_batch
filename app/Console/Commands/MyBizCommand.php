<?php
// 修正
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;
use App\Account;
use App\Services\MyBizService;
use App\Services\MyBizBuzzService;

class MyBizCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mybiz:analyse {api=none} {from=none} {to=none} {key?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'GMBデータ分析';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = $this->argument('api');
        $from = $this->argument('from');
        $to = $this->argument('to');
        $key = $this->argument('key');
        $this->_debug('MyBizCommand api=' .$api .' key=' .$key);

        $myBizService = new MyBizService();

        if ($api == 'reviews') {
            $myBizBuzzService = new MyBizBuzzService();
            // クチコミを抽出してS3にCSVデータとして格納
            if ($key == '') {
                // 契約企業全てのブランド・全店舗のクチコミを抽出
                $myBizBuzzService->analyseReviews($myBizService, $from, $to, null, null);

            } else {
                $keyAry = explode("/", $key);
                if (count($keyAry) == 1) {
                    // 指定されたブランド配下の全店舗のクチコミを抽出
                    $gmbAccountId = $keyAry[0];
                    $myBizBuzzService->analyseReviews($myBizService, $from, $to, $gmbAccountId, null);
                } else if (count($keyAry) == 2) {
                    // 指定された店舗のクチコミを抽出
                    $gmbAccountId = $keyAry[0];
                    $gmbLocationId = $keyAry[1];
                    $myBizBuzzService->analyseReviews($myBizService, $from, $to, $gmbAccountId, $gmbLocationId);
                }
            }
        }

    }

    private function _debug($msg) {
        var_dump($msg);
    }
}
